Aaron Pua
A00926321
Set 1D

Journal Entry 9


I think it is very hard to be able to land an idea job straight away after graduation. It is possible but the chances are slim. My ideal job is to develop web and mobile applications. Some of the options in CST second year offer are geared in that direction. For example, the web and mobile option should offer some training needed to go down that path.

When I graduate, I am able to work any job that comes my way, even help desk jobs. It is not really the experience in the particular area, but experience overall nonetheless. As I build up my resume. I hope to be able work in my ideal job.

Creating android applications requires strong knowledge of java which I am not too proficient at. I would have to pay more attention on java so that I would have an easier time trying to develop applications.